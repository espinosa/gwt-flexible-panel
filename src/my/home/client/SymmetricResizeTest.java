package my.home.client;

import my.home.client.widgets.resizablepanel.ResizablePanel;
import my.home.client.widgets.resizablepanel.symmetric.SymmetricActiveElementsDetectionQueue;
import my.home.client.widgets.utils.PositioningUtils;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;

public class SymmetricResizeTest implements Command {
	private final Panel rootPanel;
	public SymmetricResizeTest(Panel rootPanel) {
		this.rootPanel = rootPanel;
	}

	@Override
	public void execute() {
		rootPanel.clear();
		ResizablePanel panel = new ResizablePanel(
				new SymmetricActiveElementsDetectionQueue());
		panel.add(new HTML(FlexiblePanelTestPage.panelContent));	
		rootPanel.add(panel);
		panel.setStyleName("main-panel");
		panel.setWidth(200);
		panel.setHeight(200);
		PositioningUtils.center(panel);
	}
}
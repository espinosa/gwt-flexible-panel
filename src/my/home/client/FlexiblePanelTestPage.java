package my.home.client;


import my.home.client.widgets.resizablepanel.ResizablePanel;
import my.home.client.widgets.utils.GlobalResizeHandler;
import my.home.client.widgets.utils.LocationUtils;
import my.home.client.widgets.utils.RootPanelResizeHandler;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Test {@link ResizablePanel} page. Display and centre one flexible panel.
 * @author espinosa, 22.10.2012
 */
public class FlexiblePanelTestPage implements EntryPoint {

	protected static final String panelContent = "FlexiblePanelTestPage - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";

	private Command classicResizeTest;
	private Command symmetricResizeTest;
	private Command movingTest;
	private Command addMovingPanelTest;
	private Command classicResizeTwoPanelsTest;

	private PopupMenu popupMenu;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		String type = LocationUtils.getUrlParameter("type", "classic");

		final Panel rootPanel = RootPanel.get();
		rootPanel.setWidth("100%");
		rootPanel.setHeight(Window.getClientHeight() + "px");
		rootPanel.getElement().getStyle().setPadding(0, Unit.PX);
		rootPanel.getElement().getStyle().setBorderWidth(0, Unit.PX);
		rootPanel.getElement().getStyle().setMargin(0, Unit.PX);

		classicResizeTest = new ClassicResizeTest(rootPanel);
		symmetricResizeTest = new SymmetricResizeTest(rootPanel);
		movingTest = new MovablePanelTest(rootPanel, false);
		addMovingPanelTest = new MovablePanelTest(rootPanel, true);
		classicResizeTwoPanelsTest = new ClassicResizeTwoPanelsTest(rootPanel);

		popupMenu = new PopupMenu();

		if (type.equals("classic")) {
			classicResizeTest.execute();
		}
		else if (type.equals("symmetric")) {
			symmetricResizeTest.execute();
		}
		else if (type.equals("moving")) {
			movingTest.execute();
		}
		else if (type.equals("two")) {
			classicResizeTwoPanelsTest.execute();
		}

		rootPanel.sinkEvents(Event.ONCONTEXTMENU);
		rootPanel.addHandler(
				new ContextMenuHandler() {
					@Override
					public void onContextMenu(ContextMenuEvent event) {
						// stop the browser from opening the context menu
						event.preventDefault();
						event.stopPropagation();
						popupMenu.setPopupPosition(event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY());
						popupMenu.show();
					}
				},
				ContextMenuEvent.getType());

		GWT.log("onModuleLoad() finished");

		Window.addResizeHandler(new RootPanelResizeHandler(rootPanel));
		Window.addResizeHandler(GlobalResizeHandler.getInstance());
	}

	public class PopupMenu extends PopupPanel {
		public class PopupMenuInner extends MenuBar {
			public PopupMenuInner() {
				super(true);
				addItem(new MenuItem("Classic resizing test", menuCommand(classicResizeTest)));
				addItem(new MenuItem("Symmetric resizing text", menuCommand(symmetricResizeTest)));
				addItem(new MenuItem("Movable panel test", menuCommand(movingTest)));
				addItem(new MenuItem("Add movable panel", menuCommand(addMovingPanelTest)));
			}
		}

		public PopupMenu() {
			// PopupPanel's constructor takes 'auto-hide' as its boolean parameter.
			// If this is set, the panel closes itself automatically when the user
			// clicks outside of it.
			super(true);

			// PopupPanel is a SimplePanel, so you have to set it's widget property to
			// whatever you want its contents to be.
			setWidget(new PopupMenuInner());
		}

		/** wrap command, just to hide menu after */
		protected Command menuCommand(final Command command) {
			return new Command() {
				@Override
				public void execute() {
					command.execute();
					PopupMenu.this.hide();
				}
			};
		}
	}
}

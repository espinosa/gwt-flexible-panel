package my.home.client.widgets.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.ui.RequiresResize;

/**
 * Intended to be registered with browser window resize notifier. Any panel
 * requiring to be called on parent container resize but its parent does not
 * unfortunately notifies its children about its resize - like AbsolutePanel,
 * RootPanel, or any dumb, CSS only driven DIV - such unfortunate child widget
 * needs to be registered with {@link GlobalResizeHandler}.
 * <p>
 * Example of usage:
 * <pre>
 * Window.addResizeHandler(GlobalResizeHandler.getInstance());
 * <pre>
 * <p>
 * Does not need to be used with children of LayoutPanel, this panel notifies its children.
 * @author espinosa
 */
public class GlobalResizeHandler implements ResizeHandler {
	private List<RequiresResize> listeners = new LinkedList<RequiresResize>();
	private static final GlobalResizeHandler instance = new GlobalResizeHandler();
	public final Logger logger = Logger.getLogger("test");
	
	protected GlobalResizeHandler() {
	}
	
	public static GlobalResizeHandler getInstance() {
		return instance;
	}
	
	public void addListener(RequiresResize resizable) {
		listeners.add(resizable);
	}

	public void onResize(ResizeEvent event) {
		logger.info("GlobalResizeHandler - onResize event");
		for (RequiresResize r : listeners) {
			r.onResize();
		}
	}
}
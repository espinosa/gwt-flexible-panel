package my.home.client.widgets.utils;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.Window;

public class LocationUtils {

	/**
	 * Example, for URL like "http://127.0.0.1:8888/index.html?gwt.codesvr=127.0.0.1:9997&type=symmetric"
	 * it returns "index.html"
	 */
	public static String getPath() {
		return Window.Location.getPath();
	}
	
	public static String getUrlParameter(String paramName, String defaultValue) {
		String result = defaultValue;
		Map<String, List<String>> paramMap = Window.Location.getParameterMap();
		List<String> values = paramMap.get(paramName);
		if (values != null) {
			String value = values.get(0);
			if (value != null) {
				result = value;
			}
		}
		return result;
	}
}

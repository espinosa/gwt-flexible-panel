package my.home.client.widgets.utils;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.ui.Panel;

/**
 * Set root panel height to 100% of This class is very handy if basic RootPanel
 * used, that is when AbsolutPanel is the root container. LayoutRootPanel does
 * that automatically but has other disadvantages regarding flexible panel.
 * <p>
 * Example of use:
 * <pre>
 * <code>
 * 	public void onModuleLoad() {
 * 		final Panel rootPanel = RootPanel.get();
 * 		Window.addResizeHandler(new RootPanelResizeHandler(rootPanel));
 * }
 * </code>
 * </pre>
 * 
 * @author espinosa
 */
public class RootPanelResizeHandler implements ResizeHandler {
	private final Panel rootPanel;
	
	public RootPanelResizeHandler(Panel rootPanel) {
		this.rootPanel = rootPanel;
	}
	
	/**
	 * Set the root container height same as browser window client height with
	 * every resize this way keeps it as 100%,that is full stretch.
	 * 
	 * Width does not have to be treated like this to achieve full stretch, It
	 * can be simply set as 100%, height cannot, weirdness of HTML positioning.
	 */
	public void onResize(ResizeEvent event) {
		int height = event.getHeight();
		rootPanel.setHeight(height + "px");
	}
}
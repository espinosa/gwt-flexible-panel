package my.home.client.widgets.utils;

import com.google.gwt.dom.client.Element;

public class DomUtils {

	// see: http://forgetmenotes.blogspot.co.uk/2009/05/gwt-disable-text-selection-in-table.html
	// see: https://groups.google.com/forum/#!msg/google-web-toolkit/OQ1eaSMFWJI/1KOaoUoyY64J

	/**
	 * Disable text selection for the given DOM Element. 
	 * Set special values to DOM properties relevant to text selection.
	 */
	public native static void disableTextSelection(Element e) /*-{
	    e.ondrag = function () { return false; };
	    e.onselectstart = function () { return false; };
	    e.style.MozUserSelect = "none";
	    e.style.webkitUserSelect = "none";
	    e.style.msUserSelect = "none";
	}-*/;
	
	/**
	 * Enable text selection for the given DOM Element.
	 * Set default values to DOM properties relevant to text selection
	 */
	public native static void enableTextSelection(Element e) /*-{
		e.ondrag = null;
		e.onselectstart = null;
		e.style.MozUserSelect = "text";
		e.style.webkitUserSelect = "text";
		e.style.msUserSelect = "text";
	}-*/;
	
	/**
	 * Disable globally text selection.
	 * UNTESTED!
	 */
	public native static void disableTextSelection() /*-{
        $doc.ondrag = function () { return false; };
        $doc.onselectstart = function () { return false; };
        $doc.style.MozUserSelect = "none";
        $doc.style.webkitUserSelect = "none";
	}-*/;

	/**
	 * Enable globally text selection.
	 * Set default values to DOM properties relevant to text selection
	 * TODO: we should store original values and return them when enabling, not setting default values
	 * UNTESTED!
	 */
	public native static void enableTextSelection() /*-{
    	$doc.ondrag = null;
    	$doc.onselectstart = null;
    	$doc.style.MozUserSelect    = "text";
    	$doc.style.webkitUserSelect = "text";
	}-*/;



}

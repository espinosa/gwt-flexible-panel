package my.home.client.widgets.resizablepanel;


public class NoActiveElement implements ActiveElement {
	public String getCursorName() {
		return "default";
	}
	public boolean isResize() {
		return false;
	}
	public boolean isMove() {
		return false;
	}
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		// no-op
	}
	public ActiveElement probe(PositionsSource ps) {
		return this;
	}
	public void setNext(ActiveElement next) {

	}
	public static NoActiveElement instance = new NoActiveElement();
}
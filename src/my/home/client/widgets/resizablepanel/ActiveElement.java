package my.home.client.widgets.resizablepanel;

/**
 * Abstract representation active parts of a DOM element, like individual 
 * resizable edges of a Panel or a draggable title bar of a Panel.
 * <p>
 * Every element have reference to the underling element.
 * <p>
 * ActiveElements are chained. Every ActiveElement knows it's next in line. See
 * {@link #probe(PositionsSource)} and {@link #setNext(ActiveElement).
 * <p>
 * ActiveElement is responsible for detecting if the cursor position is relevant
 * to it, see {@link #probe(PositionsSource)}. If not, next element in the chain is
 * probed.
 * <p>
 * ActiveElements elements currently supports two actions, two modes - resize
 * and move - for the underlying element. It is associated with exactly one. See
 * {@link #isMove()} and {@link #isResize()}.
 * <p>
 * ActiveElement is capable of reshape the underlying element.
 * It keeps the knowledge how to do it depending on current cursor position.
 * For this purpose the underlying element must implement {@link Reshapable}.
 * Example: Left edge resizes panel differently then bottom edge.
 * <p>
 * TODO: split chain related methods to a different class - probe() and setNext()
 * 
 * @author espinosa
 */
public interface ActiveElement {
	
	/**
	 * When mouse hovers over represented part of an element it can change shape
	 * Example: hovering over East edge of a resizable panel, cursor should have
	 * vertical resize shape.
	 * <p>
	 * See for values:
	 * http://www.javascriptkit.com/dhtmltutors/csscursors.shtml
	 * 
	 * @return cursor name like 'e-resize', 'w-resize', 'se-resize', ..
	 */
	String getCursorName();
	
	/** 
	 * @return true if active element represents resizing the underlying element.
	 */
	boolean isResize();
	
	/** 
	 * @return true if active element represents moving the underlying element.
	 */
	boolean isMove();
	
	/**
	 * Reshape the underlying element
	 * @param r abstractized underlying element. Usually it is a Panel but it can be anything implementing Reshapable.
	 * @param cursorX
	 * @param cursorY
	 */
	void reshape(Reshapable r, int cursorX, int cursorY);
	
	/**
	 * Probe chain of active elements.
	 * If this element is not matching, call probe in next element in the chain.
	 * Last in chain should be {@link NoActiveElement}.
	 * which is returned when no matching element is found.
	 * 
	 * @param ps
	 *            abstractized cursor position
	 * @return
	 */
	ActiveElement probe(PositionsSource ps);
	
	/**
	 * Set next active element, build the chain
	 * @param next
	 */
	void setNext(ActiveElement next);
}
package my.home.client.widgets.resizablepanel;


/**
 * Represents East (right) edge of a Panel. 
 */
public class ResizeActiveElementSE extends BaseResizeActiveElement {
	
	public ResizeActiveElementSE(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		return 
				(ps.cursorX() > (ps.left() + ps.width() - ps.resizableEdgeSize()))
				&&
				(ps.cursorX() < (ps.left() + ps.width()))
				&&
				(ps.cursorY() > (ps.top() + ps.height() - ps.resizableEdgeSize()))
				&& 
				(ps.cursorY() < (ps.top() + ps.height()))	
				;	
	}
	
	@Override
	public String getCursorName() {
		return "se-resize";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(
				core, 
				ps.width() + ps.left() - ps.cursorX(), 
				ps.top() + ps.height() - ps.cursorY()
				);
	}
	
	@Override
	public String toString() {
		return "ResizeE [" + super.reshaper.getClass().getName() + "]";
	}
}
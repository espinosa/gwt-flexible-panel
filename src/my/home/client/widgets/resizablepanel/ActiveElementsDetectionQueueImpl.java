package my.home.client.widgets.resizablepanel;


public class ActiveElementsDetectionQueueImpl implements ActiveElementsDetectionQueue {
	private ActiveElement first, last;
	
	@Override
	public ActiveElementsDetectionQueue add(ActiveElement newElement) {
		if (first==null) {
			first = last = newElement;
		}
		else {
			last.setNext(newElement);
			last = newElement;
		}
		return this;
	}
	
	@Override
	public ActiveElementsDetectionQueue add(ActiveElementsDetectionQueue elementQueue) {
		return add(elementQueue.getFirst());
	}
	
	@Override
	public ActiveElement getFirst() {
		return first;
	}
	
	@Override
	public ActiveElement probe(PositionsSource ps) {
		return first.probe(ps);
	}
}
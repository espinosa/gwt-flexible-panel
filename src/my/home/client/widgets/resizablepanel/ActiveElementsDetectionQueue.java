package my.home.client.widgets.resizablepanel;


public interface ActiveElementsDetectionQueue {
	ActiveElementsDetectionQueue add(ActiveElement activeElement);
	ActiveElement probe(PositionsSource ps);
	ActiveElementsDetectionQueue add(ActiveElementsDetectionQueue elementQueue);
	ActiveElement getFirst();
}
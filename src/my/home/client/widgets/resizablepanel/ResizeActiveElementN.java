package my.home.client.widgets.resizablepanel;


/**
 * Represents North (top) edge of a Panel sensitive to resizing in that direction. 
 */
public class ResizeActiveElementN extends BaseResizeActiveElement {
	
	public ResizeActiveElementN(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		return (
				(ps.cursorY() > (ps.top()))
				&& 
				(ps.cursorY() < (ps.top() + ps.resizableEdgeSize()))
				);	
	}
	
	@Override
	public String getCursorName() {
		return "n-resize";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(core, 0, ps.top() - ps.cursorY());
	}
	
	@Override
	public String toString() {
		return "ResizeE [" + super.reshaper.getClass().getName() + "]";
	}
}
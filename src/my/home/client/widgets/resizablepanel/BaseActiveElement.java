package my.home.client.widgets.resizablepanel;



public abstract class BaseActiveElement implements ActiveElement {
	
	private ActiveElement next = NoActiveElement.instance;
	
	protected ReshapeCommand reshaper;
	
	/**
	 * Main constructor
	 * 
	 * @param reshaperActiveElement
	 *            holds reference to reshaping command which in turn has
	 *            reference to Reshapable (underlying GUI element)
	 */
	public BaseActiveElement(ReshapeCommand reshaper) {
		this.reshaper = reshaper;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCursorName() {
		return "default";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override	
	public boolean isResize() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override	
	public boolean isMove() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		reshaper.reshape(r, cursorX, cursorY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ActiveElement probe(PositionsSource ps) {
		if (check(ps)) return getInstance(this, ps);
		else return next.probe(ps);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setNext(ActiveElement next) {
		this.next = next;
	}

	/**
	 * Check if the given (cursor) position is relevant for this element.
	 * Called by {@link #probe(PositionsSource)}
	 * 
	 * @param ps
	 * @return true if the given (cursor) position is relevant for this element.
	 */
	public abstract boolean check(PositionsSource ps);
	
	/**
	 * Factory method with default implementation returning this ActiveElement.
	 * Subclasses can override this method to provide more state information. 
	 * Like adding offset data.
	 * 
	 * TODO: get rid of this approach, it is too confusing and contaminates object responsibilities.
	 * 
	 * @param core
	 * @param ps
	 * @return
	 */
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return core;
	}
}
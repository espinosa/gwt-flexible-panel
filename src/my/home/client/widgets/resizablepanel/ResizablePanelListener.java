package my.home.client.widgets.resizablepanel;

public interface ResizablePanelListener {
	public void onResized(int newLeft, int newTop, int newWidth, int newHeight);
}

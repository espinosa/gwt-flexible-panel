package my.home.client.widgets.resizablepanel;

public interface Centerable {
	void center();
}

package my.home.client.widgets.resizablepanel;


/**
 * Represents West (left) edge of a Panel. 
 */
public class ResizeActiveElementW extends BaseResizeActiveElement {
	
	public ResizeActiveElementW(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		return ((ps.cursorX() > ps.left()) && (ps.cursorX() < ps.left() + ps.resizableEdgeSize()));	
	}
	
	@Override
	public String getCursorName() {
		return "w-resize";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 * 
	 * @param core
	 * @param ps
	 * @return
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(core, ps.cursorX() - ps.left(), 0);
	}
	
	@Override
	public String toString() {
		return "ResizeR [" + super.reshaper.getClass().getName() + "]";
	}
}
package my.home.client.widgets.resizablepanel;



public abstract class BaseResizeActiveElement extends BaseActiveElement {
	
	public BaseResizeActiveElement(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean isResize() {
		return true;
	}
}
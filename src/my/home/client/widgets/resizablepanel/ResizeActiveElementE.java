package my.home.client.widgets.resizablepanel;


/**
 * Represents East (right) edge of a Panel. 
 */
public class ResizeActiveElementE extends BaseResizeActiveElement {
	
	public ResizeActiveElementE(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		return (ps.cursorX() > ps.left() + ps.width() - ps.resizableEdgeSize() && ps.cursorX() < ps.left() + ps.width());	
	}
	
	@Override
	public String getCursorName() {
		return "e-resize";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(core, ps.width() + ps.left() - ps.cursorX(), 0);
	}
	
	@Override
	public String toString() {
		return "ResizeE [" + super.reshaper.getClass().getName() + "]";
	}
}
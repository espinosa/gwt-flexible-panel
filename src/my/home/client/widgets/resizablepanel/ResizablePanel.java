package my.home.client.widgets.resizablepanel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import my.home.client.widgets.pospanel.PosCake2Panel;
import my.home.client.widgets.resizablepanel.classic.ClassicActiveElementsDetectionQueue;
import my.home.client.widgets.utils.DomUtils;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.RequiresResize;

/**
 * Flexible panel main class. Build upon easily positionable Panel. Adds
 * reshaping capabilities reacting on certain mouse events. Flexible design
 * enables to define which parts of the panel reacts on events and how they
 * change the panel. See {@link ActiveElementsDetectionQueue} for detecting
 * active parts of the panel and resizing strategies.
 * <p>
 * By defining active elements queue you can define only south-west corner to
 * react, or any side, or just one edge, or the size of resizing edge.
 * <p>
 * This design enables defining for example asymmetric (classic) or symmetric
 * (eccentric) resizing. Both basic implementations are included.
 * <p>
 * Moving of whole panel is defined usually by a child item, there can be
 * several of them or it can be something completely else, like top edge. Again,
 * it is very flexible.
 * <p>
 * The design also enables adding various animated effects to accompany resize,
 * like bounce backs rubber edges effects.
 * <p>
 * Panel is also capable of reacting on browser window resize or parent
 * container resize - this enables thing like always centred panel.
 * <p>
 * By reshaping is meant both resize and move, that is change of any of its 2
 * coordinates.
 * 
 * @author espinosa
 */
public class ResizablePanel extends PosCake2Panel implements Reshapable, RequiresResize {
	private boolean elementLock = false;
	private List<ResizablePanelListener> panelResizedListeners = new ArrayList<ResizablePanelListener>();

	public ResizablePanel() {
		this(null);
	}
	
	public ResizablePanel(ActiveElementsDetectionQueue activeElementsDetectionQueue) {
		super();

		//listen to mouse-events
		sinkEvents(
				Event.ONMOUSEDOWN |
				Event.ONMOUSEMOVE | 
				Event.ONMOUSEUP |
				Event.ONMOUSEOVER
				);
		
		if (activeElementsDetectionQueue!=null) {
			queue = activeElementsDetectionQueue; 
		} else {
			queue = new ClassicActiveElementsDetectionQueue();
		}
	}
	
	private final ActiveElementsDetectionQueue queue; 
	
	PositionsSource ps;
	ActiveElement activeElement;

	/**
	 * Main method. Called by the sinked event, events we listen to. Cause
	 * cursor change, indicating active area, or reshaping of the panel itself.
	 * During the dragging disable annoying text selection. 
	 */
	@Override
	public void onBrowserEvent(Event event) {
		final int eventType = DOM.eventGetType(event);

		ps = new PositionsSource(event, this);
		
		if (!elementLock) {
			activeElement = queue.probe(ps);
		}

		if (Event.ONMOUSEOVER == eventType || Event.ONMOUSEMOVE == eventType) {
			DOM.setStyleAttribute(this.getElement(), "cursor", activeElement.getCursorName());
		}

		if (Event.ONMOUSEDOWN == eventType) {
			if (activeElement.isResize() || activeElement.isMove()) {
				elementLock = true;
				DOM.setCapture(this.getElement());
				DomUtils.disableTextSelection(this.getElement());
			}
		} 

		else if (Event.ONMOUSEMOVE == eventType) {
			if (elementLock) {
				activeElement.reshape(this, ps.cursorX(), ps.cursorY());
			}
		} 

		else if (Event.ONMOUSEUP == eventType) {
			if (elementLock) {
				elementLock = false;
				DOM.releaseCapture(this.getElement());
				DomUtils.enableTextSelection(this.getElement());
			}
		}
	}

	/**
	 * Define reshape of this panel. This method is called by various
	 * 'reshapers', the reshaping strategies. It is called usually upon user
	 * interaction.
	 * <p>
	 * It notifies listeners. It is important for proportional positioning
	 * towards parent container or in browser window.
	 */
	@Override
	public void reshape(int newLeft, int newTop, int newWidth, int newHeight) {
		setPixelSize(newLeft, newTop, Math.abs(newWidth), Math.abs(newHeight));
		notifyPanelResizedListeners(newLeft, newTop, newWidth, newHeight);
	}

	/**
	 * Interface function to add a listener to this event
	 * @param listener
	 */
	public void addPanelResizedListener(ResizablePanelListener listener) {
		panelResizedListeners.add(listener);
	}

	/**
	 * Notify all registered listeners about panel reshaping 
	 */
	private void notifyPanelResizedListeners(int newLeft, int newTop, int newWidth, int newHeight) {
		for (Iterator<ResizablePanelListener> i = panelResizedListeners.iterator(); i.hasNext(); ) {
			((ResizablePanelListener) i.next()).onResized(newLeft, newTop, newWidth, newHeight);
		}
	}
	
	// TODO: better name
	public class NullParentResizeStrategy implements RequiresResize {
		@Override
		public void onResize() {
			// do nothing
		}
	}
	
	private RequiresResize parentResizeStrategy = new NullParentResizeStrategy();
	

	public RequiresResize getParentResizeStrategy() {
		return parentResizeStrategy;
	}

	public void setParentResizeStrategy(RequiresResize parentResizeStrategy) {
		this.parentResizeStrategy = parentResizeStrategy;
	}

	@Override
	public void onResize() {
		parentResizeStrategy.onResize(); 
		// it is not enough to implement this method. It only works when parent container support
		// Resize event propagation. LayoutPanel does, AbsolutePanel does not. And AbsolutePanel is what
		// I recommend to use.
		// Instead of implementing onResize(), I recommend registration with (my) GlobalResizeHandler (singleton)
		// GlobalResizeHandler needs to be registered itself with Window.addResizeHandler().
		// TODO: consider not to implement RequiresResize::onResize() at all - Espinosa, 5.11.2012
	}
}

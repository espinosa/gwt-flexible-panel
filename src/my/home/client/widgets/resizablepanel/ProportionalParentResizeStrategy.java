package my.home.client.widgets.resizablepanel;

import java.util.logging.Logger;

import my.home.client.widgets.utils.GlobalResizeHandler;

import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;

/**
 * Browser (or parent container) resize strategy for a flexible Panel
 * {@link ResizablePanel}. Keep panel position, that is left top coordinate
 * proportioned with every browser window resize. Prominent usage is for
 * centred panels, they are kept centred with every browser resize.
 * <p>
 * This strategy does registration for browser resize events with
 * {@link GlobalResizeHandler}.
 * <p>
 * Example usage:
 * <pre>
 * ProportionalParentResizeStrategy proportionalParentResizeStrategy = new ProportionalParentResizeStrategy(panel);
 * panel.setParentResizeStrategy(proportionalParentResizeStrategy);
 * </pre>
 *  
 * @author espinosa
 */
public class ProportionalParentResizeStrategy implements RequiresResize {
	public final Logger logger = Logger.getLogger(ProportionalParentResizeStrategy.class.getName());
	
	/**
	 * Relative position of the panel in relation to its parent container. Value
	 * 0.5 means that 'centre' of the Panel is inexact half of the parent width
	 * That means panel is perfectly centred.
	 */
	private float relativeLeft = 0;
	
	/**
	 * Relative position of the panel in relation to its parent container. Value
	 * 0.5 means that 'centre' of the Panel is inexact half of the parent width
	 * That means panel is perfectly centred.
	 */
	private float relativeTop  = 0;
	
	/**
	 * Panel reference
	 */
	private final ResizablePanel panel;
	
	/**
	 * Constructor. Attach this handler to given Panel.
	 * @param panel
	 */
	public ProportionalParentResizeStrategy(ResizablePanel panel) {
		logger.fine("ProportionalParentResizeStrategy create");
		this.panel = panel;
		// register itself with Panel reshape notifier 
		panel.addPanelResizedListener(new UpdateRelativePositionOnEveryPanelReshaping());
		// register itself with GlobalResizeHandler (itself registered with global browser window resize handler)
		GlobalResizeHandler.getInstance().addListener(ProportionalParentResizeStrategy.this);
	}
	
	/**
	 * When user (what else?) resizes or moves Panel, its relative position must be recomputed.
	 */
	protected class UpdateRelativePositionOnEveryPanelReshaping implements ResizablePanelListener {
		@Override
		public void onResized(int newLeft, int newTop, int newWidth, int newHeight) {
			final Widget p = panel.getParent();
			// recalculate relative top and left based on parent width/height and half panel width andheight (panel centre)
			int parentWidth = p.getOffsetWidth();
			int parentHeight = p.getOffsetHeight();
			relativeLeft = parentWidth > 0  ? ((float)newLeft + (float)newWidth/2f) / (float)parentWidth   : 0;  
			relativeTop  = parentHeight > 0 ? ((float)newTop  + (float)newHeight/2f) / (float)parentHeight : 0;
		}
	}
	
	/**
	 * Call back method for {@link GlobalResizeHandler}. On browser window
	 * resize the position of Panel is recomputed, the absolute position based
	 * on stored relative position.
	 */
	@Override
	public void onResize() {
		logger.fine("ProportionalParentResizeStrategy - recalculatePosition()");
		Widget p = panel.getParent();
		int newLeft = Math.round((float)p.getOffsetWidth() * relativeLeft - panel.getWidth()/2f);
		int newTop  = Math.round((float)p.getOffsetHeight() * relativeTop - panel.getHeight()/2f);
		panel.setLeft(newLeft);
		panel.setTop(newTop);
	}
}
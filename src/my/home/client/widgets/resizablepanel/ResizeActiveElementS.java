package my.home.client.widgets.resizablepanel;


/**
 * Represents South (bottom) edge of a Panel sensitive to resizing in that direction. 
 */
public class ResizeActiveElementS extends BaseResizeActiveElement {
	
	public ResizeActiveElementS(ReshapeCommand rc) {
		super(rc);
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		return (
				(ps.cursorY() > (ps.top() + ps.height() - ps.resizableEdgeSize()))
				&& 
				(ps.cursorY() < (ps.top() + ps.height()))
				);	
	}
	
	@Override
	public String getCursorName() {
		return "s-resize";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(core, 0, ps.top() + ps.height() - ps.cursorY());
	}
	
	@Override
	public String toString() {
		return "ResizeE [" + super.reshaper.getClass().getName() + "]";
	}
}
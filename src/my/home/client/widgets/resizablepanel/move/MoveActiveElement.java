package my.home.client.widgets.resizablepanel.move;


import my.home.client.widgets.resizablepanel.ActiveElement;
import my.home.client.widgets.resizablepanel.BaseActiveElement;
import my.home.client.widgets.resizablepanel.PositionsSource;
import my.home.client.widgets.resizablepanel.ReshapeCommand;
import my.home.client.widgets.resizablepanel.ResizeActiveElementWithOffset;

import com.google.gwt.dom.client.Element;


/**
 * Represents Element inside parent Panel responsible for moving whole parent panel when dragged.  
 */
public class MoveActiveElement extends BaseActiveElement {
	
	private final Element element;
	
	public MoveActiveElement(ReshapeCommand rc, Element element) {
		super(rc);
		this.element = element;
	}
	
	@Override
	public boolean check(PositionsSource ps) {
		Element targetElement = ps.getEventElement();
		do {
			if (targetElement.equals(element)) return true;
		} while ((targetElement = targetElement.getParentElement()) != null);
		return false;
	}
	
	@Override
	public String getCursorName() {
		return "move";
	}
	
	/**
	 * Factory method with default implementation returning stateful ActiveElement with relevant offset data
	 */
	@Override
	protected ActiveElement getInstance(ActiveElement core, PositionsSource ps) {
		return new ResizeActiveElementWithOffset(core, ps.left() - ps.cursorX(), ps.top() - ps.cursorY());
	}
	
	@Override
	public String toString() {
		return "Move [" + super.reshaper.getClass().getName() + "]";
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override	
	public boolean isMove() {
		return true;
	}
}
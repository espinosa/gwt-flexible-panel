package my.home.client.widgets.resizablepanel.move;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** move panel command. */
public class MoveCommand implements ReshapeCommand {
	/** the cursor X and Y represents a , they are offset adjusted, it is not very intuitive */
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		r.reshape(cursorX, cursorY, r.getWidth(), r.getHeight());
	}
}
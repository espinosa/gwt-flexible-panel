package my.home.client.widgets.resizablepanel.classic;

import my.home.client.widgets.resizablepanel.ActiveElementsDetectionQueueImpl;
import my.home.client.widgets.resizablepanel.ResizeActiveElementE;
import my.home.client.widgets.resizablepanel.ResizeActiveElementN;
import my.home.client.widgets.resizablepanel.ResizeActiveElementS;
import my.home.client.widgets.resizablepanel.ResizeActiveElementSE;
import my.home.client.widgets.resizablepanel.ResizeActiveElementW;

public class ClassicActiveElementsDetectionQueue extends ActiveElementsDetectionQueueImpl {
	
	public ClassicActiveElementsDetectionQueue() {
		this
		.add(new ResizeActiveElementSE(new ReshapeCommandSE()))
		.add(new ResizeActiveElementE( new ReshapeCommandE()))
		.add(new ResizeActiveElementW( new ReshapeCommandW()))
		.add(new ResizeActiveElementN( new ReshapeCommandN()))
		.add(new ResizeActiveElementS( new ReshapeCommandS()));
	}
}

package my.home.client.widgets.resizablepanel.classic;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - East (that is right) side only */
public class ReshapeCommandE implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		r.reshape(r.getLeft(), r.getTop(), cursorX - r.getLeft(), r.getHeight());
	}
}
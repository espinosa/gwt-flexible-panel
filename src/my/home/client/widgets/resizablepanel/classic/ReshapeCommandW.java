package my.home.client.widgets.resizablepanel.classic;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - West (that is left) side only */
public class ReshapeCommandW implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		r.reshape(cursorX, r.getTop(), r.getWidth() + (r.getLeft() - cursorX), r.getHeight());
	}
}
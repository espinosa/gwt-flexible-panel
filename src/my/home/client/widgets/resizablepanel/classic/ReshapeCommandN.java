package my.home.client.widgets.resizablepanel.classic;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - North side only */
public class ReshapeCommandN implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		r.reshape(r.getLeft(), cursorY, r.getWidth(), r.getHeight() + (r.getTop() - cursorY));
	}
}
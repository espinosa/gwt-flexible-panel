package my.home.client.widgets.resizablepanel;

public class ResizeEventHandler {
	private int resizableEdgeSize = 5;
	
	protected boolean isResizeW(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return (cursorX > 0 && cursorX < resizableEdgeSize );
	}
	
	protected boolean isResizeE(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return (cursorX > left + width - resizableEdgeSize && cursorX < left + width);
	}
	
	protected boolean isResizeN(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return (cursorY > 0 && cursorY < resizableEdgeSize);
	}
	
	protected boolean isResizeS(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return (cursorY > top + height - resizableEdgeSize && cursorY < top + height);
	}
	
	protected boolean isResizeSE(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return isResizeS(cursorY, cursorX, top, left, height, width) && isResizeE(cursorY, cursorX, top, left, height, width);
	}
	
	protected boolean isResizeNE(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return isResizeN(cursorY, cursorX, top, left, height, width) && isResizeE(cursorY, cursorX, top, left, height, width);
	}
	
	protected boolean isResizeNW(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return isResizeN(cursorY, cursorX, top, left, height, width) && isResizeW(cursorY, cursorX, top, left, height, width);
	}
	
	protected boolean isResizeSW(
			int cursorY, int cursorX,
			int top, int left,
			int height, int width) 
	{
		return isResizeS(cursorY, cursorX, top, left, height, width) && isResizeW(cursorY, cursorX, top, left, height, width);
	}
}

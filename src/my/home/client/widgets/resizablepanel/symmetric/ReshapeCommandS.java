package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - South side only */
public class ReshapeCommandS implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		int difference = cursorY - (r.getTop() + r.getHeight());
		r.reshape(r.getLeft(), r.getTop() - difference, r.getWidth(), r.getHeight() + 2*difference);
	}
}
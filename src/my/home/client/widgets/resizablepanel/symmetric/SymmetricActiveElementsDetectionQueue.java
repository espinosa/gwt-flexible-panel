package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.ActiveElementsDetectionQueueImpl;
import my.home.client.widgets.resizablepanel.ResizeActiveElementE;
import my.home.client.widgets.resizablepanel.ResizeActiveElementN;
import my.home.client.widgets.resizablepanel.ResizeActiveElementS;
import my.home.client.widgets.resizablepanel.ResizeActiveElementSE;
import my.home.client.widgets.resizablepanel.ResizeActiveElementW;

/**
 * Active element detection queue attached to symmetric reshapers. The panel
 * will be resized equally on the opposite side too. Eccentric resizing.
 * 
 * @author espinosa
 */
public class SymmetricActiveElementsDetectionQueue extends ActiveElementsDetectionQueueImpl {{
	this
	.add(new ResizeActiveElementSE(new ReshapeCommandSE()))
	.add(new ResizeActiveElementE( new ReshapeCommandE()))
	.add(new ResizeActiveElementW( new ReshapeCommandW()))
	.add(new ResizeActiveElementN( new ReshapeCommandN()))
	.add(new ResizeActiveElementS( new ReshapeCommandS()));
}}

package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - East (that is right) side only */
public class ReshapeCommandE implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		int difference = cursorX - (r.getLeft() + r.getWidth());
		r.reshape(r.getLeft() - difference, r.getTop(), r.getWidth() + 2*difference, r.getHeight());
	}
}
package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - South side only */
public class ReshapeCommandSE implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		int differenceX = cursorX - (r.getLeft() + r.getWidth());
		int differenceY = cursorY - (r.getTop() + r.getHeight());
		r.reshape(r.getLeft() - differenceX, r.getTop() - differenceY, r.getWidth() + 2*differenceX, r.getHeight() + 2*differenceY);
	}
}
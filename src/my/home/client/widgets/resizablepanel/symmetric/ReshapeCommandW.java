package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - West (that is left) side only */
public class ReshapeCommandW implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		int difference = r.getLeft() - cursorX;
		r.reshape(r.getLeft() - difference, r.getTop(), r.getWidth() + 2*difference, r.getHeight());
	}
}
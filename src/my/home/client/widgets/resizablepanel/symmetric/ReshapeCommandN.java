package my.home.client.widgets.resizablepanel.symmetric;

import my.home.client.widgets.resizablepanel.Reshapable;
import my.home.client.widgets.resizablepanel.ReshapeCommand;

/** reshape command - North side only */
public class ReshapeCommandN implements ReshapeCommand {
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		int difference = r.getTop() - cursorY;
		r.reshape(r.getLeft(), r.getTop() - difference, r.getWidth(), r.getHeight() + 2*difference);
	}
}
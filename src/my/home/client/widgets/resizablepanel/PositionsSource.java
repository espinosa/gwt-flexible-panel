package my.home.client.widgets.resizablepanel;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Panel;

public class PositionsSource {
	private Event event;
	private Panel panel;
	private Element eventElement;
	
	public PositionsSource(Event event, Panel panel) {
		this.event = event;
		this.panel = panel;
		this.eventElement = event.getEventTarget().cast();
	}
	public int cursorX() {
		return DOM.eventGetClientX(event);
	}
	public int cursorY() {
		return DOM.eventGetClientY(event);
	}
	public int top() {
		return panel.getAbsoluteTop();
	}
	public int left() {
		return panel.getAbsoluteLeft();
	}
	public int height() {
		return panel.getOffsetHeight();
	}
	public int width() {
		return panel.getOffsetWidth();
	}
	public int resizableEdgeSize() {
		return 20;
	}
	public Element getEventElement() {
		return eventElement;
	}
}
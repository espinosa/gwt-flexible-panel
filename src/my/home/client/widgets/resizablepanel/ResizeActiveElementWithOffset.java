package my.home.client.widgets.resizablepanel;

/**
 * Decorator. Delegating decorator. Wrapper around original ActiveElement
 * instance, delegating calls to underlying original {@link ActiveElement}
 * instance. adding offset data (state data) necessary for some resizers.
 * Effectively immutable.
 * 
 * @author espinosa
 */
public class ResizeActiveElementWithOffset implements ActiveElement {
	private final ActiveElement origin;
	private final int offsetX;
	private final int offsetY;
	
	
	public ResizeActiveElementWithOffset(ActiveElement origin, int offsetX, int offsetY) {
		this.origin = origin;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	
	@Override
	public String getCursorName() {
		return origin.getCursorName();
	}
	
	@Override
	public boolean isResize() {
		return origin.isResize();
	}
	
	@Override
	public boolean isMove() {
		return origin.isMove();
	}
	
	@Override
	public void reshape(Reshapable r, int cursorX, int cursorY) {
		origin.reshape(r, cursorX + offsetX, cursorY + offsetY);
	}
	
	@Override
	public ActiveElement probe(PositionsSource ps) {
		throw new RuntimeException("probe() should never be called for ActiveElement wrappers, only original ActiveElement");
		// TODO: amend original interface
	}
	
	@Override
	public void setNext(ActiveElement next) {
		throw new RuntimeException("setNext() should never be called for ActiveElement wrappers, only original ActiveElement");
		// TODO: amend original interface
	}

	@Override
	public String toString() {
		return "ResizeActiveElementWithOffset [origin=" + origin + ", offsetX="
				+ offsetX + ", offsetY=" + offsetY + "]";
	}
}

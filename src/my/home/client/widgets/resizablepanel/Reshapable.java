package my.home.client.widgets.resizablepanel;

import com.google.gwt.user.client.ui.Widget;

public interface Reshapable {
	void reshape(int newLeft, int newTop, int newWidth, int newHeight);
	int getLeft();
	int getTop();
	int getWidth();
	int getHeight();
	Widget getParent();
	void addPanelResizedListener(ResizablePanelListener listener);
}

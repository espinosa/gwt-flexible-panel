package my.home.client;

import my.home.client.widgets.resizablepanel.ProportionalParentResizeStrategy;
import my.home.client.widgets.resizablepanel.ResizablePanel;
import my.home.client.widgets.utils.PositioningUtils;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;

public class ClassicResizeTest implements Command {
	private final Panel rootPanel;
	public ClassicResizeTest(Panel rootPanel) {
		this.rootPanel = rootPanel;
	}

	@Override
	public void execute() {
		rootPanel.clear();
		ResizablePanel panel = new ResizablePanel();
		panel.add(new HTML(FlexiblePanelTestPage.panelContent));	
		rootPanel.add(panel);
		panel.setStyleName("main-panel");
		panel.setWidth(200);
		panel.setHeight(200);
		ProportionalParentResizeStrategy proportionalParentResizeStrategy = new ProportionalParentResizeStrategy(panel);
		panel.setParentResizeStrategy(proportionalParentResizeStrategy);
		PositioningUtils.center(panel);
	}
}
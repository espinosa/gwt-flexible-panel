package my.home.client;

import my.home.client.widgets.resizablepanel.ResizablePanel;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;

public class ClassicResizeTwoPanelsTest implements Command {
	private final Panel rootPanel;
	public ClassicResizeTwoPanelsTest(Panel rootPanel) {
		this.rootPanel = rootPanel;
	}

	@Override
	public void execute() {
		rootPanel.clear();

		ResizablePanel panel1 = new ResizablePanel();
		panel1.add(new HTML(FlexiblePanelTestPage.panelContent));	
		rootPanel.add(panel1);
		panel1.setStyleName("main-panel");
		panel1.setPixelSize(10, 10, 200, 200);

		ResizablePanel panel2 = new ResizablePanel();
		panel2.add(new HTML(FlexiblePanelTestPage.panelContent));	
		rootPanel.add(panel2);
		panel2.setStyleName("main-panel");
		panel2.setPixelSize(300, 10, 200, 200);
	}
}
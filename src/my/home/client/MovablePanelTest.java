package my.home.client;

import my.home.client.widgets.resizablepanel.ActiveElementsDetectionQueueImpl;
import my.home.client.widgets.resizablepanel.ResizablePanel;
import my.home.client.widgets.resizablepanel.classic.ClassicActiveElementsDetectionQueue;
import my.home.client.widgets.resizablepanel.move.MoveActiveElement;
import my.home.client.widgets.resizablepanel.move.MoveCommand;
import my.home.client.widgets.utils.PositioningUtils;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;

public class MovablePanelTest implements Command {
	private final Panel rootPanel;
	private final boolean addMode;

	public MovablePanelTest(Panel rootPanel, boolean addMode) {
		this.rootPanel = rootPanel;
		this.addMode = addMode;
	}

	@Override
	public void execute() {
		if (!addMode) rootPanel.clear();
		final Panel titleBar = titleBar("drag me..");
		ResizablePanel panel = new ResizablePanel(
				new ActiveElementsDetectionQueueImpl() {{
					this
					.add(new MoveActiveElement(new MoveCommand(), titleBar.getElement()))
					.add(new ClassicActiveElementsDetectionQueue());
				}});
		panel.add(titleBar);
		panel.add(new HTML(FlexiblePanelTestPage.panelContent));	
		rootPanel.add(panel);
		panel.setStyleName("main-panel");
		panel.setWidth(200);
		panel.setHeight(300);
		PositioningUtils.center(panel);
	}

	private AbsolutePanel titleBar(String title) {
		final AbsolutePanel titleBar = new AbsolutePanel();
		titleBar.add(new HTML(title));
		titleBar.addStyleName("title-bar");
		Style s = titleBar.getElement().getStyle();
		s.setMarginTop(5, Unit.PX);
		s.setMarginLeft(5, Unit.PX);
		s.setMarginRight(5, Unit.PX);
		s.setMarginBottom(5, Unit.PX);
		return titleBar;
	}
}
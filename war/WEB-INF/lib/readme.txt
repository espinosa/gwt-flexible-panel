Copy to this directory GWT/GAE libraries if you want it running under GAE server:
appengine-api-1.0-sdk-1.7.2.1.jar
appengine-api-labs.jar
appengine-endpoints.jar
appengine-jsr107cache-1.7.2.1.jar
datanucleus-appengine-1.0.10.final.jar
datanucleus-core-1.1.5.jar
datanucleus-jpa-1.1.5.jar
geronimo-jpa_3.0_spec-1.1.1.jar
geronimo-jta_1.1_spec-1.1.1.jar
gwt-servlet.jar
jdo2-api-2.3-eb.jar
jsr107cache-1.1.jar

TODO: figure out which of there libraries are really needed
TODO: figure out how to make Google Eclipse plugin not complaining aboutmissing libraries in WEB-INF/lib and use those in classpath (they are already there! provided by the very same plugin, grrr)